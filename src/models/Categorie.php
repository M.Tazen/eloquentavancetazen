<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 02/01/2019
 * Time: 19:51
 */

namespace catawich\models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table      = 'categorie';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    public function sandwichs(){
        return $this->belongsToMany(
            'catawich\models\Sandwich',
            'sand2cat',
            'cat_id',
            'sand_id');
    }

}

