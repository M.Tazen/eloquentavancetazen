<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 20/11/2018
 * Time: 11:45
 */

namespace catawich\models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table      = 'image';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = ['titre'];

    public function sandwich(){
        return $this->belongsTo('catawich\models\Sandwich', 's_id');
    }

}