<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 20/11/2018
 * Time: 09:52
 */

namespace catawich\models;

use Illuminate\Database\Eloquent\Model;

class Sandwich extends Model
{
    protected $table      = 'sandwich';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    public function images(){
        return $this->hasMany('catawich\models\Image', 's_id');
    }

    public function categories(){
        return $this->belongsToMany(
            'catawich\models\Categorie',
            'sand2cat',
            'sand_id',
            'cat_id');
    }

    public function tailles(){
        return $this->belongsToMany(
            'catawich\models\TailleSandwich',
            'tarif',
            'sand_id',
            'taille_id')
            ->withPivot( ['prix'] );
    }

}



