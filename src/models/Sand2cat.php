<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 02/01/2019
 * Time: 19:57
 */

namespace catawich\models;

use Illuminate\Database\Eloquent\Model;

class Sand2cat extends Model
{
    protected $table      = 'sand2cat';
    protected $primaryKey = 'id';
    public    $timestamps = false;

}