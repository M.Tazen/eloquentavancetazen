<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 10/01/2019
 * Time: 16:52
 */

namespace catawich\models;
use Illuminate\Database\Eloquent\Model;


class TailleSandwich extends Model
{

    protected $table      = 'taille_sandwich';
    protected $primaryKey = 'id';
    public    $timestamps = false;


    public function sandwichs(){
        return $this->belongsToMany(
            'catawich\models\Sandwich',
            'tarif',
            'taille_id',
            'sand_id')
            ->withPivot( ['prix'] )
            ->as( 'tarification' );
    }

}