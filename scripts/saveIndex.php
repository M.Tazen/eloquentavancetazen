<?php



/*
********************************************** 1. Requêtes simples *************************************************
1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain,
2. idem, en triant selon le type_pain,
3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
Utiliser l'exception ModelNotFoundException.
4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain
5. créer un nouveau sandwich et l'insérer dans la base.

 */


/* 1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain, */
foreach (Sandwich::all() as $sand){
    echo $sand->nom .", " .$sand->description .", " .$sand->type_pain ."<br>";
}


/* 2. idem, en triant selon le type_pain, */
echo "<br><br>-------------------------------<br>";
foreach (Sandwich::select()->orderBy('type_pain')->get() as $sand){
    echo $sand->nom .", " .$sand->type_pain ."<br>";
}

/* 3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
Utiliser l'exception ModelNotFoundException. */
echo "<br><br>------------throw Exception-------------------<br>";
try{
    $idSand = 42;
    $sand = Sandwich::find($idSand);
    if ($sand){
        echo "le sandwich " .$idSand . " s'appelle " .$sand->nom;
    } else {
        throw new ModelNotFoundException("le sandwich " .$idSand ." n'existe pas");
    }
} catch (ModelNotFoundException $e){
    echo $e->getMessage();
}

echo "<br><br>---------------firstOrFail----------------<br>";
try{
    $idSand = 42;
    //$sand = Sandwich::find($idSand);
    $sand = Sandwich::where('id', '=', $idSand)->firstOrFail();

} catch (ModelNotFoundException $e){
    echo $e->getMessage();
}


/* 4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain */
echo "<br><br>-------------------------------<br>";
$list = Sandwich::where('type_pain', 'like', '%baguette%')
    ->orderBy('type_pain')
    ->get();
foreach ($list as $sand){
    echo $sand->nom .", " .$sand->type_pain ."<br>";
}

/* 5. créer un nouveau sandwich et l'insérer dans la base. */
echo "<br><br>-------------------------------<br>";

$monSandwich = new Sandwich();
$monSandwich->nom = "Le Serpentard";
$monSandwich->description = " que des méchants légumes dedans ";
$monSandwich->type_pain = " mie ";

try{
    if($monSandwich->save()){
        echo "sandwich added";
    } else {
        throw new ModelNotFoundException("le sandwich n'a pas été enregistré");
    }
} catch (ModelNotFoundException $e){
    echo $e->getMessage();
}


/*
 * ***********************************************QUESTION DEUX*********************************************************************
1. afficher le sandwich n°4 et lister les images associées,
2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
liste des images associées. Utiliser un chargement lié.
3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
son type de pain.
4. créer 3 images associées au sandwich ajouté dans l'exercice 1.
5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6
*/


/* 1. afficher le sandwich n°4 et lister les images associées,*/
echo "<br><br>--------------Exo 2 - 1 : -------------------<br>";

$idSand = 4;
$sand = Sandwich::find($idSand);
$images = $sand->images;

echo "le sandwich " .$idSand . " s'appelle " .$sand->nom;
foreach ($images as $img){
    echo $img->titre ."<br>";
}


/* 2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
liste des images associées. Utiliser un chargement lié. */
echo "<br><br>--------------Exo 2 - 2 : -------------------<br>";

$sandwiches = Sandwich::select()
    ->with('images')
    ->orderBy('type_pain')
    ->get();

foreach ($sandwiches as $sand){
    echo $sand->nom ."<br> Titre images : " ;
    //var_dump($sand);
    //var_dump($sand['images']);
    foreach ($sand['images'] as $img){
        echo $img->titre ." ";
    }
    echo"<br><br>";
}

/* 3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
son type de pain. */
echo "<br><br>--------------Exo 2 - 3 : -------------------<br>";

$images = Image::select()
    ->with('sandwich')
    ->get();

foreach ($images as $img){
    echo $img->titre ." : " .$img['sandwich']->nom ."<br>";
}

/* 4. créer 3 images associées au sandwich ajouté dans l'exercice 1. */
echo "<br><br>--------------Exo 2 - 4 : -------------------<br>";
echo "(créer trois images)";
$leSerpentard = Sandwich::find(11);

//for ($i = 0; $i < 3; $i++) {
//    echo "ok" .$i ."<br>";
//    $image = new Image();
//    $image->titre = "serpentard_" .$i;
//    $image->type = "image/png";
//    $image->def_x = "1650";
//    $image->def_y = "600";
//    $image->taille = "15468";
//    $image->filename = "img_5a045cd771388.png";
//
//    //$leSerpentard->images()->associate($image)->save($image);
//    $image->sandwich()->associate($leSerpentard)->save([$leSerpentard]);
//    echo "ok " .$image->titre ."<br>";
//}

/* 5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6 */
echo "<br><br>--------------Exo 2 - 5 : -------------------<br>";

$sandwich6 = Sandwich::find(4);
$sandwichm = Sandwich::find(5);

$imageToChange = Image::select()
    ->with('sandwich')
    ->where('s_id', '=', $leSerpentard->id)
    ->orderBy('titre', 'desc')
    ->first();

//$res = Image::updateOrCreate(['titre' => 'www'])->sandwich()->associate($sandwichm)->save();

echo $imageToChange->titre ."<br>";
$imageToChange->sandwich()->associate($sandwich6)->save();






/*
 ***************************** QUESTION 3 - Associations N-N ********************************
1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
pain) et le nom de chacune des catégories auxquelles il est associé.
2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
utiliser un chargement lié.
3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.
4. associer le sandwich créé au 1.5 aux catégories 1 et 3.
 * */


/* 1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
pain) et le nom de chacune des catégories auxquelles il est associé. */

$sand = Sandwich::find(5);
$categs = $sand->categories()->get();

echo "le sandwich 5 s'appelle " .$sand->nom .".<br> Ses catégories sont :<br>";
foreach ($categs as $cat){
    echo $cat->nom ."<br>";
}



/*2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
utiliser un chargement lié.*/

$categs2 = Categorie::with('sandwichs')->get();

echo "<br><br>------------<br><br> 3.2/ les categories et leur sandwichs (chargement lié) <br>";

foreach ($categs2 as $cat){
    echo "---" .$cat->nom ."---<br>";
    foreach ($cat->sandwichs as $sand){
        echo $cat->nom ." : " .$sand->nom ."<br>";
    }
}


/*3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.*/

echo "<br><br>------------<br><br> 3.3/ les sandwichs de type baguettes, leur image et leur categories (chargement lié) <br>";


$sandouichs =
    Sandwich::with('categories')
        ->where('type_pain', 'like', "%baguette%")
        ->get();

foreach ($sandouichs as $sa){
    echo "---" .$sa->nom ."---<br>";
    foreach ($sa->categories as $cate){
        echo $sa->nom ." : " .$cate->nom ."<br>";
    }
    foreach ($sa->images as $img){
        echo $sa->nom ." : " .$img->filename ."<br>";
    }
}


/* 4. associer le sandwich créé au 1.5 aux catégories 1 et 3 */

echo "3.4/ ajout de l'association entre le nouveau sandwich et les categ 1 et 3 ";
echo "(code commenté)";
//$serpent = Sandwich::find('11');
//$serpent->categories()->attach( [1,3] );




/*
 * 4. Attributs d'associations
 * 1. afficher la liste des tailles proposées pour le sandwich d'ID 5,
 * 2. idem, mais en ajoutant le prix du sandwich pour chaque taille,
 * 3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans
 * chaque cas.
 */


/* 1. afficher la liste des tailles proposées pour le sandwich d'ID 5,*/
echo " <br><br> -----------------------";
echo " <br> 1. afficher la liste des tailles proposées pour le sandwich d'ID 5<br> ";

$sandCinq = Sandwich::with('tailles')->find('5');
echo "le sandwich 5 appelé " .$sandCinq->nom ." a les tailles suivantes : <br>";
foreach ($sandCinq->tailles()->get() as $taille){
    echo " - " .$taille->nom ."<br> ";
}


echo " <br><br> -----------------------";
echo " <br>2. idem, mais en ajoutant le prix du sandwich pour chaque taille<br> ";

foreach ($sandCinq->tailles()->get() as $taille){
    echo " - " .$taille->nom ." : " .$taille->pivot->prix ."<br>";
}


echo " <br><br> -----------------------";
echo " <br>3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans chaque cas. ";
echo " <br>(code commenté) ";
/*serpent = Sandwich::find('11');
foreach (TailleSandwich::all() as $taille){
    $serpent->tailles()->attach( [
        $taille->id => ['prix'=> (mt_rand(2*10, 8*10) / 10)]
    ] );
}
*/












