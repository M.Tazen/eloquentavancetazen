<?php
require_once '../vendor/autoload.php';

use catawich\models\Sandwich;
use catawich\models\Categorie;
use catawich\models\TailleSandwich;
use catawich\models\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;

$config = parse_ini_file ('../conf/db.conf.ini');

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */





/*
1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain,
2. idem, en triant selon le type_pain,
3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
Utiliser l'exception ModelNotFoundException.
4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain
5. créer un nouveau sandwich et l'insérer dans la base.
 */


echo " <br><br><br><br><br> ";
echo " <b>--------------------------- 1. Requêtes simples -----------------------</b><br> ";


echo " <br><br> -----------------------";
echo " <br><b> 1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain,</b><br> ";

foreach (Sandwich::all() as $sand){
    echo "<b>" . $sand->nom ."</b>: 
        <br>- description : " .$sand->description ."
         <br>- type : " .$sand->type_pain ."<br>";
}


echo " <br><br> -----------------------";
echo " <br><b> 2. idem, en triant selon le type_pain,</b><br> ";

foreach (Sandwich::select()->orderBy('type_pain', 'desc')->get() as $sand){
    echo $sand->nom .", " .$sand->type_pain ."<br>";
}


echo " <br><br> -----------------------";
echo " <br><b> 3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
Utiliser l'exception ModelNotFoundException.</b><br> ";


echo "<br>---throw Exception---<br>";
try{
    $idSand = 42;
    $sand = Sandwich::find($idSand);
    if ($sand){
        echo "le sandwich " .$idSand . " s'appelle " .$sand->nom;
    } else {
        throw new ModelNotFoundException("le sandwich " .$idSand ." n'existe pas");
    }
} catch (ModelNotFoundException $e){
    echo $e->getMessage();
}

echo "<br>---firstOrFail---<br>";
try{
    $idSand = 42;
    $sand = Sandwich::where('id', '=', $idSand)->firstOrFail();

} catch (ModelNotFoundException $e){
    echo $e->getMessage();
}


echo " <br><br> -----------------------";
echo " <br><b> 4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain</b><br> ";

$list = Sandwich::where('type_pain', 'like', '%baguette%')
    ->orderBy('type_pain')
    ->get();
foreach ($list as $sand){
    echo $sand->nom .", " .$sand->type_pain ."<br>";
}


echo " <br><br> -----------------------";
echo " <br><b> 5. créer un nouveau sandwich et l'insérer dans la base.</b><br> ";
echo "(code commenté)";

//$monSandwich = new Sandwich();
//$monSandwich->nom = "Le Serpentard";
//$monSandwich->description = "que des méchants légumes";
//$monSandwich->type_pain = "mie";
//
//try{
//    if($monSandwich->save()){
//        echo "Le sandwich a été créé";
//    } else {
//        throw new Exception("le sandwich n'a pas été enregistré");
//    }
//} catch (Exception $e){
//    echo $e->getMessage();
//}





/*
1. afficher le sandwich n°4 et lister les images associées,
2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
liste des images associées. Utiliser un chargement lié.
3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
son type de pain.
4. créer 3 images associées au sandwich ajouté dans l'exercice 1.
5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6
*/


echo " <br><br><br><br><br> ";
echo " <b>---------------------------  2 - Associations 1-N -----------------------</b><br> ";



echo " <br><br> -----------------------";
echo " <br><b> 1. afficher le sandwich n°4 et lister les images associées,</b><br> ";

$idSand = 4;
$sand = Sandwich::find($idSand);
$images = $sand->images;

echo "le sandwich " .$idSand . " s'appelle " .$sand->nom ."<br>";
foreach ($images as $img){
    echo $img->titre ."<br>";
}



echo " <br><br> -----------------------";
echo " <br><b> 2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
liste des images associées. Utiliser un chargement lié.</b><br> ";

$sandwiches = Sandwich::select()
    ->with('images')
    ->orderBy('type_pain')
    ->get();

foreach ($sandwiches as $sand){
    echo $sand->nom ." (type pain : " .$sand->type_pain .")<br> " ;
    echo "Images :<br> " ;
    foreach ($sand['images'] as $img){
        echo '- ' .$img->titre ."<br> ";
    }
    echo"<br>";
}


echo " <br><br> -----------------------";
echo " <br><b> 3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
son type de pain.</b><br> ";

$images = Image::select()
    ->with('sandwich')
    ->get();

foreach ($images as $img){
    echo $img->titre ." : " .$img['sandwich']->nom .", " .$img['sandwich']->type_pain ."<br>";
}


echo " <br><br> -----------------------";
echo " <br><b> 4. créer 3 images associées au sandwich ajouté dans l'exercice 1.</b><br> ";

echo "(code commenté)";
$leSerpentard = Sandwich::find(11);
//
//for ($i = 0; $i < 3; $i++) {
//    echo .$i ."<br>";
//    $image = new Image();
//    $image->titre = "serpentard_" .$i;
//    $image->type = "image/png";
//    $image->def_x = "1650";
//    $image->def_y = "600";
//    $image->taille = "15468";
//    $image->filename = "img_5a045cd771388.png";
//
//    //$leSerpentard->images()->associate($image)->save($image);
//    $image->sandwich()->associate($leSerpentard)->save([$leSerpentard]);
//    echo "image créée : " .$image->titre ."<br>";
//}



echo " <br><br> -----------------------";
echo " <br><b> 5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6 </b><br> ";
echo "(code commenté)";

//$sandwich6 = Sandwich::find(6);
//
//$imageToChange = Image::select()
//    ->with('sandwich')
//    ->where('s_id', '=', $leSerpentard->id)
//    ->orderBy('titre', 'desc')
//    ->first();
//
//$imageToChange->sandwich()->associate($sandwich6)->save();









/*
1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
pain) et le nom de chacune des catégories auxquelles il est associé.
2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
utiliser un chargement lié.
3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.
4. associer le sandwich créé au 1.5 aux catégories 1 et 3.
 * */


echo " <br><br><br><br><br> ";
echo " <b>---------------------------  3 - Associations N-N -----------------------</b><br> ";


echo " <br><br> -----------------------";
echo " <br><b> 1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
pain) et le nom de chacune des catégories auxquelles il est associé.</b><br> ";


$sand = Sandwich::find(5);
$categs = $sand->categories()->get();

echo
    "le sandwich 5 : <br>"
    ."- nom : ".$sand->nom ."<br>"
    ."- description : ".$sand->description ."<br>"
    ."- type_pain : ".$sand->type_pain ."<br>"

    ."<br> Ses catégories sont :<br>";
foreach ($categs as $cat){
    echo "- " .$cat->nom ."<br>";
}



echo " <br><br> -----------------------";
echo " <br><b> 2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
utiliser un chargement lié.</b><br> ";

$categs2 = Categorie::with('sandwichs')->get();


foreach ($categs2 as $cat){
    echo "---" .$cat->nom ."---<br>";
    foreach ($cat['sandwichs'] as $sand){
        echo $cat->nom ." : " .$sand->nom ."<br>";
    }
}


echo " <br><br> -----------------------";
echo " <br><b> 3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.</b><br> ";

$sandouichs =
    Sandwich::with(['categories', "images"])
        ->where('type_pain', 'like', "%baguette%")
        ->get();

foreach ($sandouichs as $sa){
    echo "<br>---$sa->nom---<br>";

    echo "Type pain  : $sa->type_pain <br>";

    echo "Categories : <br>";
    foreach ($sa['categories'] as $cate){
        echo "- $cate->nom <br>";
    }

    echo "Images : <br>";
    foreach ($sa['images'] as $img){
        echo "- $img->titre <br>";
    }
}


echo " <br><br> -----------------------";
echo " <br><b> 4. associer le sandwich créé au 1.5 aux catégories 1 et 3.</b><br> ";
echo "(code commenté)";
//$serpent = Sandwich::find('11');
//$serpent->categories()->attach( [1,3] );







/*
 * 4. Attributs d'associations
 * 1. afficher la liste des tailles proposées pour le sandwich d'ID 5,
 * 2. idem, mais en ajoutant le prix du sandwich pour chaque taille,
 * 3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans
 * chaque cas.
 */

echo " <br><br><br><br><br> ";
echo " <b>---------------------------  4. Attributs d'associations -----------------------</b><br> ";



echo " <br><br> -----------------------";
echo " <br><b> 1. afficher la liste des tailles proposées pour le sandwich d'ID 5</b><br> ";

$sandCinq = Sandwich::with('tailles')->find('5');
echo "Tailles du sandwich 5 ($sandCinq->nom) : <br>";
foreach ($sandCinq->tailles()->get() as $taille){
    echo " - " .$taille->nom ."<br> ";
}


echo " <br><br> -----------------------";
echo " <br><b>2. idem, mais en ajoutant le prix du sandwich pour chaque taille</b><br> ";

foreach ($sandCinq->tailles()->get() as $taille){
    echo " - " .$taille->nom ." : " .$taille->pivot->prix ."<br>";
}



echo " <br><br> -----------------------";
echo " <br><b>3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans chaque cas. </b>";
echo " <br>(code commenté) ";
//$serpent = Sandwich::find('11');
//foreach (TailleSandwich::all() as $taille){
//    $serpent->tailles()->attach( [
//        $taille->id => ['prix'=> (mt_rand(2*10, 8*10) / 10)]
//    ] );
//}









/*
 *  5. Requêtes sur des associations
1. pour la catégorie dont le nom contient 'traditionnel', lister les sandwichs dont le type_pain
contient 'baguette',
2. pour le sandwich d'ID 5, lister les images de type 'image/jpeg' et de def_X > 720,
3. lister les sandwichs qui ont plus de 4 images associées,
4. lister les catégories qui ont plus de 6 images associées,
5. lister les catégories qui contiennent des sandwichs dont le type de pain est 'baguette',
6. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000
7. lister les catégories qui possèdent des images de types 'image/jpeg' de taille > 18000
8. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000 et qui
sont de catégorie 'traditionnel',
9. pour le sandwich d'ID 7, lister les tailles pour lequel il est disponible avec un prix < 7.0
 */

echo " <br><br><br><br><br> ";
echo " <b>--------------------------- 5. Requêtes sur des associations -----------------------</b><br> ";


echo " <br><br> -----------------------";
echo " <br><b>1. pour la catégorie dont le nom contient 'traditionnel', lister les sandwichs dont le type_pain
contient 'baguette'</b><br> ";


$cat = Categorie::where("nom", "like", "%traditionnel%")->first();
echo 'la catégorie ' .$cat->nom . " a les sandwichs baguettes : <br> ";
$list = $cat->sandwichs()
                ->where('type_pain', 'like', '%baguette%')
                ->get();
foreach ($list as $sand){
    echo " - $sand->nom ($sand->type_pain) <br>";
}


echo " <br><br> -----------------------";
echo " <br><b>2. pour le sandwich d'ID 5, lister les images de type 'image/jpeg' et de def_X > 720</b><br> ";
$sandCinq = Sandwich::find('5');
$listImg = $sandCinq->images()
    ->where('type', '=', 'image/jpeg')
    ->where('def_X', '>', '720')
    ->get();
foreach ($listImg as $im){
    echo ' - ' .$im->filename . " <br>";
}



echo " <br><br> -----------------------";
echo " <br><b>3. lister les sandwichs qui ont plus de 4 images associées</b><br> ";
$sandQuatreImg = Sandwich::has('images', '>', 4)->get();
foreach ($sandQuatreImg as $sand){
    echo ' - ' .$sand->nom . " <br>";
}



echo " <br><br> -----------------------";
echo " <br><b>4. lister les catégories qui ont plus de 6 images associées</b><br> ";
//$catSixImg =
//    Categorie::
//        with('sandwichs')
//        ->has('images', '>', 6)
//        ->get();
echo '.';


echo " <br><br> -----------------------";
echo " <br><b>5. lister les catégories qui contiennent des sandwichs dont le type de pain est 'baguette'</b><br> ";
$categBaguette =
    Categorie::
        whereHas('sandwichs', function($q) {
            $q->where('type_pain', '=', 'baguette');
        })->get();
foreach ($categBaguette as $ca){
    echo ' - ' .$ca->nom . " <br>";
}


echo " <br><br> -----------------------";
echo " <br><b>6. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000</b><br> ";
$sandJpeg18k =
    Sandwich::
        whereHas('images', function($q) {
            $q->where('type', '=', 'image/jpeg')
                ->where('taille', '>', 18000);
        })->get();
foreach ($sandJpeg18k as $sand){
    echo ' - ' .$sand->nom . " <br>";
}


echo " <br><br> -----------------------";
echo " <br><b>7. lister les catégories qui possèdent des images de types 'image/jpeg' de taille > 18000</b><br> ";
//$categJpeg18k =
//    Categorie::
//        whereHas('sandwichs', function($q) {
//            $q->whereHas('images', function($i) {
//                $i->where('type', '=', 'image/jpeg')
//                    ->where('taille', '>', 18000);
//            })->get();
//        })->get();
//foreach ($categJpeg18k as $cat){
//    echo ' - ' .$cat->nom . " <br>";
//}
echo ".";




echo " <br><br> -----------------------";
echo " <br><b>8. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000 et qui
sont de catégorie 'traditionnel'</b><br> ";
$sandJpeg18k =
    Sandwich::
        whereHas('images', function($q) {
            $q->where('type', '=', 'image/jpeg')
                ->where('taille', '>', 18000);
        })
        ->whereHas('categories', function($q) {
            $q->where('nom', '=', 'traditionnel');
        })
        ->get();
foreach ($sandJpeg18k as $sand){
    echo ' - ' .$sand->nom . " <br>";
}





echo " <br><br> -----------------------";
echo " <br><b>9. pour le sandwich d'ID 7, lister les tailles pour lequel il est disponible avec un prix < 7.0</b><br> ";
//$tailleSandSept =
//    TailleSandwich::
//        whereHas('sandwichs', function($q) {
//            $q->where('id', '=', 7)
//                ->wherePivot('prix', '<', 7);
//        })
//        ->get();
//
//foreach ($tailleSandSept as $taille){
//    echo ' - ' .$taille->nom . " <br>";
//}



echo " <br><br><br><br><br><br> ";




